import { WOODEMI_PREFIX, WoodemiClient } from 'woodemi/WoodemiClient'

Uint8Array.prototype.startWith = function (prefix) {
  if (this.length < prefix.length) return false

  for (let i in prefix) {
    if (this[i] != prefix[i]) return false
  }
  return true
}

let NotepadHelper = {
  support(notepadScanResult) {
    if (notepadScanResult.manufacturerData.startWith(WOODEMI_PREFIX)) {
      return true
    }
    return false
  },

  create(notepadScanResult) {
    if (notepadScanResult.manufacturerData.startWith(WOODEMI_PREFIX)) {
      return new WoodemiClient()
    }
  }
}

export {
  NotepadHelper
}