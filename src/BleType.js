import regeneratorRuntime from 'wx-promise-pro'

class BleType {
  constructor(deviceId) {
    this.deviceId = deviceId
  }

  async setNotifiable({ serviceId, characteristicId }, state) {
    return await wx.pro.notifyBLECharacteristicValueChange({
      deviceId: this.deviceId,
      serviceId,
      characteristicId,
      state
    })
  }

  async writeValue({ serviceId, characteristicId }, valueArray) {
    return await wx.pro.writeBLECharacteristicValue({
      deviceId: this.deviceId,
      serviceId,
      characteristicId,
      value: valueArray.buffer
    })
  }
}

export default BleType
