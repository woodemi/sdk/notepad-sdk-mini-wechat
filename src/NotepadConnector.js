import regeneratorRuntime from 'wx-promise-pro'
import { NotepadHelper } from 'Notepad'
import NotepadType from 'NotepadType'
import BleType from 'BleType'
import { AccessDeniedError, AccessUnconfirmedError } from 'Errors'

const ConnectionState = Object.freeze({
  Disconnected: Symbol('Disconnected'),
  Connecting: Symbol('Connecting'),
  AwaitConfirm: Symbol('AwaitConfirm'),
  Connected: Symbol('Connected')
})

const DisconnectionCause = Object.freeze({
  Unknown: Symbol('Unknown'),
  Unauthorized: Symbol('Unauthorized'),
  Unconfirmed: Symbol('Unconfirmed')
})

// Private property
let supported
let enabled
let scanning

let notepadScanResult
let notepadClient
let notepadType

let NotepadConnector = {
  async init() {
    let { platform } = await wx.pro.getSystemInfo()
    supported = platform != 'devtools'
    if (!supported) return

    wx.onBluetoothAdapterStateChange(onBluetoothAdapterStateChange)
    wx.onBluetoothDeviceFound(onBluetoothDeviceFound)
    wx.onBLEConnectionStateChange(onBLEConnectionStateChange)
    wx.onBLECharacteristicValueChange(onBLECharacteristicValueChange)

    await wx.pro.openBluetoothAdapter()
    await this.refreshBluetoothAdapterState()
  },

  isSupported() {
    return supported
  },

  async close() {
    await wx.pro.closeBluetoothAdapter()
  },

  async refreshBluetoothAdapterState() {
    let { available, discovering } = await wx.pro.getBluetoothAdapterState()
    enabled = available
    scanning = discovering
  },

  isEnabled() {
    return enabled
  },

  isScanning() {
    return scanning
  },

  onNotepadFound: null,

  async startScan() {
    console.info('startScan')
    if (!supported) throw 'BLE unsupported'

    if (!enabled) {
      // TODO Timeout
      await this.refreshBluetoothAdapterState()
      if (!enabled) throw 'BLE disabled'
    }

    if (!scanning)
      await wx.pro.startBluetoothDevicesDiscovery()
  },

  async stopScan() {
    console.info('stopScan')
    if (!supported) throw 'BLE unsupported'

    if (scanning)
      await wx.pro.stopBluetoothDevicesDiscovery()
  },

  onConnectionStateChange: null,

  async connect(scanResult) {
    console.info('connect')
    notepadScanResult = scanResult
    notepadClient = NotepadHelper.create(notepadScanResult)
    notepadType = new NotepadType(notepadClient, new BleType(notepadScanResult.deviceId))
    await wx.pro.createBLEConnection({ deviceId: notepadScanResult.deviceId })

    if (this.onConnectionStateChange) {
      let state = ConnectionState.Connecting
      this.onConnectionStateChange({ notepadClient, state })
    }
  },

  async disconnect() {
    console.info('disconnect')
    await wx.pro.closeBLEConnection({ deviceId: notepadScanResult.deviceId })
  },

  async prepareConnection() {
    console.debug('prepareConnection')
    try {
      await this.discoverServices(notepadClient.inputServices)
      await notepadType.configCharacteristics()
      await notepadClient.completeConnection()
      if (this.onConnectionStateChange) {
        this.onConnectionStateChange({
          notepadClient,
          state: ConnectionState.Connected
        })
      }
    } catch (e) {
      let state = ConnectionState.Disconnected
      let cause = e === AccessDeniedError ? DisconnectionCause.Unauthorized
        : e === AccessUnconfirmedError ? DisconnectionCause.Unconfirmed
          : DisconnectionCause.Unknown

      if (this.onConnectionStateChange) {
        this.onConnectionStateChange({ notepadClient, state, cause })
      }
    }
  },

  async discoverServices(intentServices) {
    console.debug('discoverServices', intentServices)
    let { services } = await wx.pro.getBLEDeviceServices({
      deviceId: notepadScanResult.deviceId
    })

    let filtered = services.filter(item => intentServices.includes(item.uuid))
    if (filtered.length < intentServices.length)
      throw `Some services not found`

    let inputServiceCharacteristics = [
      ...notepadClient.inputIndicationCharacteristics,
      ...notepadClient.inputNotificationCharacteristics
    ]
    await Promise.all(intentServices.map(s => NotepadConnector.confirmCharacteristics(s, inputServiceCharacteristics)))
  },

  async confirmCharacteristics(serviceId, inputServiceCharacteristics) {
    let intentCharacteristics = inputServiceCharacteristics
      .filter(item => item.serviceId == serviceId)
      .map(item => item.characteristicId)
    let { characteristics } = await wx.pro.getBLEDeviceCharacteristics({
      deviceId: notepadScanResult.deviceId,
      serviceId: serviceId
    })

    let filtered = characteristics.filter(item => intentCharacteristics.includes(item.uuid))
    if (filtered.length < intentCharacteristics.length)
      throw `Some characteristics not found`
  }
}

function onBluetoothAdapterStateChange({ available, discovering }) {
  console.debug(`onBluetoothAdapterStateChange ${available} ${discovering}`)
  enabled = available
  scanning = discovering
}

function onBluetoothDeviceFound({ devices }) {
  console.debug(`onBluetoothDeviceFound ${devices.length}`)

  if (!NotepadConnector.onNotepadFound) return

  for (let d of devices) {
    let { name, deviceId, advertisData, RSSI } = d
    let manufacturerData = new Uint8Array(advertisData)
    let notepadScanResult = { name, deviceId, manufacturerData, RSSI }
    if (NotepadHelper.support(notepadScanResult)) {
      NotepadConnector.onNotepadFound(notepadScanResult)
    }
  }
}

function onBLEConnectionStateChange({ deviceId, connected }) {
  console.debug(`onBLEConnectionStateChange ${deviceId}, ${connected}`)
  if (connected) {
    NotepadConnector.prepareConnection()
  } else {
    if (NotepadConnector.onConnectionStateChange) {
      NotepadConnector.onConnectionStateChange({
        notepadClient,
        state: ConnectionState.Disconnected,
        cause: DisconnectionCause.Unknown
      })
    }
  }
}

function onBLECharacteristicValueChange({ characteristicId, value }) {
  let valueArray = new Uint8Array(value)
  console.debug(`onBLECharacteristicValueChange ${characteristicId} ${valueArray.join()}`)
  notepadType.inputValueChannel.emit(characteristicId, valueArray)
}

export {
  NotepadConnector,
  ConnectionState,
  DisconnectionCause
}