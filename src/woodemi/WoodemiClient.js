import regeneratorRuntime from 'wx-promise-pro'
import NotepadClient from '../NotepadClient'
import { AccessDeniedError, AccessUnconfirmedError } from '../Errors'

const WOODEMI_PREFIX = new Uint8Array([87, 68, 77])

const SERV__COMMAND = "57444D01-BA5E-F4EE-5CA1-EB1E5E4B1CE0"
const CHAR__COMMAND_REQUEST = "57444E02-BA5E-F4EE-5CA1-EB1E5E4B1CE0"
const CHAR__COMMAND_RESPONSE = CHAR__COMMAND_REQUEST

const AccessResult = Object.freeze({
  Denied: Symbol('Denied'),           // Device claimed by other user
  Confirmed: Symbol('Confirmed'),     // Access confirmed, indicating device not claimed by anyone
  Unconfirmed: Symbol('Unconfirmed'), // Access unconfirmed, as user doesn't confirm before timeout
  Approved: Symbol('Approved')        // Device claimed by this user
})

class WoodemiClient extends NotepadClient {
  get commandRequestCharacteristic() {
    return { serviceId: SERV__COMMAND, characteristicId: CHAR__COMMAND_REQUEST }
  }

  get commandResponseCharacteristic() {
    return { serviceId: SERV__COMMAND, characteristicId: CHAR__COMMAND_RESPONSE }
  }

  get inputIndicationCharacteristics() {
    return [
      this.commandResponseCharacteristic
    ]
  }

  get inputNotificationCharacteristics() {
    return []
  }

  async completeConnection() {
    let accessResult = await this.checkAccess([0x00, 0x00, 0x00, 0x01], 10)
    if (accessResult === AccessResult.Denied) {
      throw 'AccessDeniedError'
    } else if (accessResult === AccessResult.Confirmed) {
      // TODO Claim authorization
    } else if (accessResult === AccessResult.Unconfirmed) {
      throw 'AccessUnconfirmedError'
    } else if (accessResult === AccessResult.Approved) {
      // Empty by design
    }
  }

  async checkAccess(authToken, seconds) {
    console.debug('checkAccess')
    let response = await this.notepadType.executeCommand({
      request: new Uint8Array([0x01, seconds, ...authToken]),
      intercept: (value) => value[0] == 0x02,
      handle: (value) => value[1]
    })

    if (response == 0x00) {
      return AccessResult.Denied
    } else if (response == 0x01) {
      let confirm = await this.notepadType.receiveResponse(
        this.commandResponseCharacteristic,
        (value) => value[0] == 0x03
      )
      return confirm[1] == 0x00 ? AccessResult.Confirmed : AccessResult.Unconfirmed
    } else if (response == 0x02) {
      return AccessResult.Approved
    } else {
      throw 'Unknown Error'
    }
  }
}

export {
  WOODEMI_PREFIX,
  WoodemiClient
}
