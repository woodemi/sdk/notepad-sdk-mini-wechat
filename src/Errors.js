const AccessDeniedError = new Error('Notepad claimed by other user')

const AccessUnconfirmedError = new Error('User doesn\'t confirm before timeout')

export {
  AccessDeniedError,
  AccessUnconfirmedError
}
