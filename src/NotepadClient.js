import regeneratorRuntime from 'wx-promise-pro'

class NotepadClient {
  constructor() {
    if (this.constructor === NotepadClient) {
      throw new TypeError('Abstract class "NotepadClient" cannot be instantiated directly.')
    }
  }

  get commandRequestCharacteristic() {
    throw 'TODO("not implemented")'
  }

  get commandResponseCharacteristic() {
    throw 'TODO("not implemented")'
  }

  get inputIndicationCharacteristics() {
    throw 'TODO("not implemented")'
  }

  get inputNotificationCharacteristics() {
    throw 'TODO("not implemented")'
  }

  get inputServices() {
    let inputServiceCharacteristics = [
      ...this.inputIndicationCharacteristics,
      ...this.inputNotificationCharacteristics
    ]
    // *Primitive type only*
    // Array.prototype.distinct() { return [...new Set(this)] }
    return [...new Set(inputServiceCharacteristics.map(item => item.serviceId))]
  }

  async completeConnection() {
    throw 'TODO("not implemented")'
  }
}

export default NotepadClient
