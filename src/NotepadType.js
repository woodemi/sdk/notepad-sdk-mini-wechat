import regeneratorRuntime from 'wx-promise-pro'
import EventEmitter from 'events'

class NotepadType {
  constructor(notepadClient, bleType) {
    this.notepadClient = notepadClient; notepadClient.notepadType = this
    this.bleType = bleType
    this._inputValueChannel = new EventEmitter()
  }

  async configCharacteristics() {
    console.debug('configCharacteristics')
    for (let sc of this.notepadClient.inputIndicationCharacteristics) {
      await this.bleType.setNotifiable(sc, true)
    }
    for (let sc of this.notepadClient.inputNotificationCharacteristics) {
      await this.bleType.setNotifiable(sc, true)
    }
  }

  // DO NOT await
  sendValue({ serviceId, characteristicId }, valueArray) {
    this.bleType.writeValue({ serviceId, characteristicId }, valueArray).then(() => {
      console.log(`onValueSend: ${characteristicId} ${valueArray.join()}`)
    })
  }

  get inputValueChannel() {
    return this._inputValueChannel
  }

  async receiveResponse({ characteristicId }, predicate) {
    let inputValueChannel = this._inputValueChannel
    return new Promise((resolve, reject) => {
      let filter = (valueArray) => {
        if (predicate(valueArray)) {
          inputValueChannel.off(characteristicId, filter)
          console.log(`onValueReceive: ${characteristicId} ${valueArray.join()}`)
          resolve(valueArray)
        }
      }
      inputValueChannel.on(characteristicId, filter)
    })
  }

  async executeCommand({ request, intercept, handle }) {
    this.sendValue(this.notepadClient.commandRequestCharacteristic, request)
    let response = await this.receiveResponse(this.notepadClient.commandResponseCharacteristic, intercept)
    return handle(response)
  }
}

export default NotepadType
