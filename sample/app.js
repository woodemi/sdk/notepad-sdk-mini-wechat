//app.js
import { NotepadConnector } from 'notepad-sdk-mini-wechat'

App({
  onLaunch: function () {
    NotepadConnector.init().catch(e => console.log('NotepadConnector init error', e))
  },
  globalData: {
    currentScanResult: null
  }
})