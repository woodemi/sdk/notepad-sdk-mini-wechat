// pages/notepad/notepad-detail.js
import regeneratorRuntime from 'wx-promise-pro'
import { NotepadConnector, ConnectionState } from 'notepad-sdk-mini-wechat'

const app = getApp()

Page({
  data: {
    scanResult: {}
  },

  onLoad: function (options) {
    this.setData({ scanResult: app.globalData.currentScanResult })
    NotepadConnector.onConnectionStateChange = this.onConnectionStateChange
  },

  bindTap: function (event) {
    switch (event.currentTarget.id) {
      case 'connect': {
        NotepadConnector.connect(this.data.scanResult).catch((e) => {
          console.debug('connect error', e)
        })
        break
      }
      case 'disconnect': {
        NotepadConnector.disconnect().catch((e) => {
          console.debug('disconnect error', e)
        })
        break
      }
    }
  },

  onConnectionStateChange: function ({ notepadClient, state, cause }) {
    if (state === ConnectionState.Disconnected) {
      console.debug(`DisconnectionCause ${cause.toString()}`)
    }
    wx.pro.showToast({ title: `${state.toString()}` })
  }
})