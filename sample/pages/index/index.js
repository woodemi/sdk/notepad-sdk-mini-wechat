//index.js
import { NotepadConnector } from 'notepad-sdk-mini-wechat'

const app = getApp()

Page({
  data: {
    scanResults: []
  },

  onLoad: function () {
    NotepadConnector.onNotepadFound = this.onNotepadFound
  },

  onShow: function () {
    // Await NotepadConnector.init()
    setTimeout(() => {
      NotepadConnector.startScan().catch((e) => console.error('startScan error', e))
    }, 500)
  },

  onHide: function () {
    NotepadConnector.stopScan().catch((e) => console.error('stopScan error', e))
  },

  onNotepadFound: function (scanResult) {
    this.data.scanResults.push(scanResult)
    this.setData({ scanResults: this.data.scanResults })
  },

  bindTapItem: function (event) {
    let index = event.currentTarget.dataset.index
    app.globalData.currentScanResult = this.data.scanResults[index]
    wx.navigateTo({
      url: '../notepad/notepad-detail',
    })
  }
})
